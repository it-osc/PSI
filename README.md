# 关于PSI

PSI是一款基于SaaS模式(Software as a Service软件即服务)的企业管理软件。PSI以商贸企业的核心业务：采购、销售、库存（进销存）为切入点，最终目标是行业化的ERP解决方案。

# PSI演示

PSI的演示见：<a target="_blank" href="https://psi.sturgeon.mopaasapp.com">https://psi.sturgeon.mopaasapp.com</a>

PC端请用`360浏览器`或者是`谷歌浏览器`访问
 
移动端扫码访问![移动端扫码访问](PSI_Mobile_URL.png)

# PSI的开源协议

PSI的开源协议为GPL v3

**不需要** 经过PSI官方授权，您就可以把PSI的全部源代码用于任何合法的商业用途，例如：
1. 给企业实施PSI
2. 自己二次开发后，销售二次开发后的产品
3. 直接销售PSI的源代码
4. 二次开发后的产品申请专利或著作权
5. 其他合法商业用途

上述商业行为中的合法获利全部归您所有，不需要给PSI支付任何费用。

# PSI相关项目


1. PSI使用帮助：https://gitee.com/crm8000/PSI_Help

2. PSI移动端：https://gitee.com/crm8000/PSI_Mobile

# 商务合作

商务合作QQ: 1569352868

二次开发服务：请把需求文档email到 1569352868@qq.com ，评估需求后会回复报价

# 技术支持

如有技术方面的问题，可以提出Issue一起讨论：https://gitee.com/crm8000/PSI/issues

# QA团队招募
1. 完善PSI目前的单元测试部分的开发
2. 加QQ：1569352868 了解详情
3. 做QA是一场马拉松，只有跑过了极点后才能体会到乐趣，请三思再动

# 致谢

PSI使用了如下开源软件，没有你们，就没有PSI
 
1、PHP (http://php.net/)

2、MySQL (http://www.mysql.com/)

3、ExtJS 4.2 (http://www.sencha.com/)

4、ThinkPHP 3.2.3 (http://www.thinkphp.cn/)

5、pinyin_php (https://gitee.com/cik/pinyin_php)

6、PHPExcel (https://github.com/PHPOffice/PHPExcel)

7、TCPDF (http://www.tcpdf.org/)

8、Material-UI (https://material-ui.com/)

9、UmiJS (https://umijs.org/)

10、DVA (https://github.com/dvajs/dva)
